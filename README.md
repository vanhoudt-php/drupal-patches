# Drupal Patches

This repository contains patches for Drupal core and specific Drupal contrib
modules that are written by Liones.
These patches are used during project provisioning via a (drush) make file in
which a reference is made to the "raw" url of the patch file (on Bitbucket).

## Directory structure

Each patch should be either in the 'core' directory if it's a Drupal core patch
or inside a subdirectory of contrib, with the name of the contrib module you're
patching.
